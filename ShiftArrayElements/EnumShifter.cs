﻿using System;

namespace ShiftArrayElements
{
    public static class EnumShifter
    {
        public static int[] Shift(int[] source, Direction[] directions)
        {
            if (source == null)
            {
                throw new ArgumentNullException(nameof(source));
            }

            if (directions == null)
            {
                throw new ArgumentNullException(nameof(directions));
            }

            for (int i = 0; i < directions.Length; i++)
            {
                switch (directions[i])
                {
                    case Direction.Left:
                        {
                            int buf = source[0];
                            for (int j = 0; j < source.Length - 1; j++)
                            {
                                source[j] = source[j + 1];
                            }

                            source[source.Length - 1] = buf;
                            break;
                        }

                    case Direction.Right:
                        {
                            for (int j = source.Length - 1; j > 0; j--)
                            {
                                source[j - 1] ^= source[j];
                                source[j] ^= source[j - 1];
                                source[j - 1] ^= source[j];
                            }

                            break;
                        }

                    default:
                        throw new InvalidOperationException($"Incorrect {directions} enum value.");
                }
            }

            return source;
        }
    }
}
